package rates.bdswiss.models;

import org.json.JSONObject;

/**
 * Created by arispanayiotou on 19/03/2018.
 */

public class RateModel {

    public String symbol;
    public double price;

    public RateModel(JSONObject jsonObject) {
        symbol = jsonObject.optString("symbol");
        price = jsonObject.optDouble("price");
    }

}
