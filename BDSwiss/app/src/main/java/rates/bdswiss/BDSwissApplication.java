package rates.bdswiss;

import android.app.Application;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by arispanayiotou on 19/03/2018.
 */

public class BDSwissApplication extends Application {

    private ThreadPoolExecutor mThreadPoolExecutor;

    private static final int NUM_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static BDSwissApplication instance;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initThreadPoolExecutor();
    }

    public static BDSwissApplication getInstance() {
        return instance;
    }

    public ThreadPoolExecutor getThreadPoolExecutor() {
        return mThreadPoolExecutor;
    }

    private void initThreadPoolExecutor() {
        mThreadPoolExecutor = new ThreadPoolExecutor(
                NUM_OF_CORES,
                NUM_OF_CORES,
                60,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());
    }


}
