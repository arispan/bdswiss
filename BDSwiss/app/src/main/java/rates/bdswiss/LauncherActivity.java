package rates.bdswiss;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import rates.bdswiss.mainFlow.MainActivity;

public class LauncherActivity extends AppCompatActivity {

    private ImageView mlauncherImageView;
    private TextView mLauncherTextView;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        mHandler = new Handler();
        initViews();
        doAnimations();
    }

    private void doAnimations() {

        mlauncherImageView.animate()
                .alpha(1)
                .setDuration(1700)
                .setStartDelay(600)
                .setInterpolator(new AccelerateInterpolator());

        mLauncherTextView.animate()
                .alpha(1)
                .setDuration(1700)
                .setStartDelay(800)
                .setInterpolator(new AccelerateInterpolator());


    }

    private void initViews() {
        mlauncherImageView = findViewById(R.id.launcherImageView);
        mLauncherTextView = findViewById(R.id.launcherTitle);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(LauncherActivity.this, MainActivity.class));
            }
        }, 2500);

    }


}
