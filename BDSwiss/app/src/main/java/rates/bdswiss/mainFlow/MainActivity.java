package rates.bdswiss.mainFlow;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rates.bdswiss.R;
import rates.bdswiss.models.RateModel;
import rates.bdswiss.utils.AppConstants;
import rates.bdswiss.utils.BDSwissUtils;
import rates.bdswiss.utils.RequestActivity;

public class MainActivity extends RequestActivity {

    private RatesAdapter mAdapter;
    private Handler handler;
    private HashMap<String, Double> ratesMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        getJsonObject(AppConstants.Urls.url);

    }

    private void initViews() {
        handler = new Handler();
        ratesMap = new HashMap<>();
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mAdapter = new RatesAdapter());
    }

    @Override
    public void onResponse(String response, String url) {
        handleSuccesResponseForRates(response);
    }

    private void handleSuccesResponseForRates(String response) {
        try {
            JSONObject tempObject = new JSONObject(response);
            JSONArray tempArray = tempObject.optJSONArray("rates");

            if (mAdapter.mRates.isEmpty()) {
                for (int i = 0; i < tempArray.length(); i++) {
                    RateModel tempModel = new RateModel(tempArray.optJSONObject(i));
                    mAdapter.mRates.add(tempModel);
                    ratesMap.put(tempModel.symbol, tempModel.price);
                }
            } else {

                addNewValuesToMap();
                mAdapter.mRates.clear();

                for (int i = 0; i < tempArray.length(); i++)
                    mAdapter.mRates.add(new RateModel(tempArray.optJSONObject(i)));

            }

            mAdapter.notifyDataSetChanged();


        } catch (JSONException e) {
            e.printStackTrace();
        }

        executeNextRequest();

    }

    private void addNewValuesToMap() {

        for (RateModel model : mAdapter.mRates)
            ratesMap.put(model.symbol, model.price);


    }

    private void executeNextRequest() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getJsonObject(AppConstants.Urls.url);
            }
        }, 10000);
    }

    @Override
    public void onError(VolleyError error, String url) {
        Toast.makeText(this, BDSwissUtils.getErrorMessage(error), Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPause() {
        if (handler != null) handler.removeCallbacksAndMessages(null);
        super.onPause();

    }

    private class RatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<RateModel> mRates = new ArrayList<>();

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RatesHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rates, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof RatesHolder) {
                ((RatesHolder) holder).rateSymbol.setText(mRates.get(position).symbol);
                ((RatesHolder) holder).rateValue.setTextColor(ContextCompat.getColor(MainActivity.this, setTextColourBasedOnValue(position)));
                ((RatesHolder) holder).rateValue.setText(String.valueOf(mRates.get(position).price));
            }
        }

        private int setTextColourBasedOnValue(int position) {
            if (ratesMap.get(mRates.get(position).symbol) > mRates.get(position).price)
                return R.color.red;
            else
                return R.color.green;
        }

        @Override
        public int getItemCount() {
            return mRates.size();
        }


        private class RatesHolder extends RecyclerView.ViewHolder {

            private TextView rateSymbol, rateValue;


            public RatesHolder(View itemView) {
                super(itemView);

                rateSymbol = itemView.findViewById(R.id.rateSymbol);
                rateValue = itemView.findViewById(R.id.rateValue);
            }
        }
    }
}
