package rates.bdswiss.utils;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by arispanayiotou on 26/07/2017.
 */

public class BDSwissJsonObjRequest extends JsonObjectRequest {

    public static final String URL = "Url";

    private HashMap<String, String> headers = new HashMap<>();

    public BDSwissJsonObjRequest(String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, new JSONObject(), listener, errorListener);
    }

    public BDSwissJsonObjRequest(int method, String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
    }

    public BDSwissJsonObjRequest(String url, JSONObject jsonRequest, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(url, jsonRequest, listener, errorListener);
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        try {
            if (response != null)
                response.put(URL, this.getUrl());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.deliverResponse(response);
    }

}
