package rates.bdswiss.utils;

/**
 * Created by arispanayiotou on 19/03/2018.
 */

public class AppConstants {

    public class Urls{
        public static final String url = "https://mt4-api-staging.herokuapp.com/rates";
    }

}
