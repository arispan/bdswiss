package rates.bdswiss.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by arispanayiotou on 20/03/2018.
 */

public abstract class RequestActivity extends AppCompatActivity implements
        RequestHelper.RequestResponseListener {


    private GeneralReqHelper helper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = new GeneralReqHelper(this);
    }


    protected void getJsonObject(String url) {
        helper.getJsonObject(url);
    }

    @Override
    protected void onStop() {
        super.onStop();
        helper.cancelAll();
    }
}
