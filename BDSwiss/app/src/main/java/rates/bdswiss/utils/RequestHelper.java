package rates.bdswiss.utils;


import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import rates.bdswiss.BDSwissApplication;

/**
 * Created by arispanayiotou on 20/03/2018.
 */

public abstract class RequestHelper {


    public interface RequestResponseListener {
        void onResponse(String response, String url);

        void onError(VolleyError error, String url);
    }

    private Map<String, Request> requestMap = new HashMap<>();
    private RequestResponseListener mListener;

    public RequestHelper(RequestResponseListener mListener) {
        this.mListener = mListener;
    }


    public void getJsonObject(String url) {
        jsonObjectReq(Request.Method.GET, url, null);
    }

    private void jsonObjectReq(int method, final String url, @Nullable JSONObject jsonObject) {
        Request request = new BDSwissJsonObjRequest(
                method,
                url,
                jsonObject == null ? new JSONObject() : jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (mListener != null) {
                            mListener.onResponse(response.toString(), requestMap.get(url).getMethod() + url);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (mListener != null) {
                    mListener.onError(error, requestMap.get(url).getMethod() + url);
                }
            }
        });

        requestMap.put(url, request);
        MySingleton.getInstance(BDSwissApplication.getInstance()).addToRequestQueue(request);
    }


    public void cancelAll() {
        for (Request t : requestMap.values())
            t.cancel();

    }


}
