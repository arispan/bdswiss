package rates.bdswiss.utils;

import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import rates.bdswiss.BDSwissApplication;
import rates.bdswiss.R;

/**
 * Created by arispanayiotou on 20/03/2018.
 */

public class BDSwissUtils {

    public static String getErrorMessage(VolleyError error) {


        if (error instanceof NoConnectionError)
            return BDSwissApplication.getInstance().getString(R.string.there_is_no_internet_connection);

        if (error instanceof TimeoutError)
            return BDSwissApplication.getInstance().getString(R.string.seems);

        if (error instanceof NetworkError)
            return BDSwissApplication.getInstance().getString(R.string.network_error);

        if (error.networkResponse == null || error.networkResponse.data == null)
            return null;


        JSONObject jsonObject = null;
        JSONArray jsonArray = null;
        try {
            jsonObject = new JSONObject(new String(error.networkResponse.data));
        } catch (JSONException ex) {
            ex.printStackTrace();
            if (error.networkResponse.statusCode == 413)
                return "File is too large";

        }

        try {
            jsonArray = new JSONArray(new String(error.networkResponse.data));
        } catch (JSONException ex) {
            ex.printStackTrace();
            if (error.networkResponse.statusCode == 413)
                return "File is too large";
        }

        if (jsonObject == null && jsonArray == null)
            return "Error";

        String message = "";

        // CASE IS JSONOBJECT
        if (jsonObject != null) {

            message = jsonObject.optString("detail");

            if (message.isEmpty()) {
                Object object1 = getResponse(jsonObject);
                if (object1 != null) {
                    if (object1 instanceof JSONArray)
                        message = ((JSONArray) object1).optString(0);
                    else {
                        Object object2 = getResponse((JSONObject) object1);
                        if (object2 != null) {
                            if (object2 instanceof JSONArray)
                                message = ((JSONArray) object2).optString(0);
                        }

                    }
                }
            }


        } else {
            message = jsonArray.optString(0);
        }

        return message.isEmpty() ? BDSwissApplication.getInstance().getString(R.string.there_was_an_error_loading_data) : message;
    }


    private static Object getResponse(JSONObject jsonObject) {
        Iterator<String> keys = jsonObject.keys();

        if (keys.hasNext()) {
            String next = keys.next();
            JSONArray array = jsonObject.optJSONArray(next);
            JSONObject object = jsonObject.optJSONObject(next);
            if (array != null)
                return array;
            else if (object != null)
                return object;
        }
        return null;
    }

}
